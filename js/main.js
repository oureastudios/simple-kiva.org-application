$(document).ready(function(){
	
	var LOADED_PROJECTS = [];
	
	$.getJSON( "http://api.kivaws.org/v1/loans/newest.json", function( data ) {
		
		// Loop through each item in the loans array
		$.each(data.loans, function(index, val) {
			// Add the loan object to the loans table using a custom function
			add_new_loan_table_item(val.id, val.name, val.use, val.status, val.sector, val.location.country, val.loan_amount, val.funded_amount, val.basket_amount);
		});
		
		// Hide the loader and show the table
		$('.loader').hide();
		$('#loans-table').show();
	});
	
	/* 
	 * Check for updates every 2 seconds
	 */
	setInterval(function(){
		$.getJSON( "http://api.kivaws.org/v1/loans/newest.json", function( data ) {
			$.each(data.loans, function(index, val) {
				// Check if the loan object has already been added to the table
				if($.inArray(val.id, LOADED_PROJECTS) === -1) {
					// Loan object has not been added already
					// Add the loan to the loans table using a custom function
					add_new_loan_table_item(val.id, val.name, val.use, val.status, val.sector, val.location.country, val.loan_amount, val.funded_amount, val.basket_amount);
				} else {
					// Loan object is already added into the table
					// Upate the loan item in the table using a custom function
					update_loan_table_item(val.id, val.name, val.use, val.status, val.sector, val.location.country, val.loan_amount, val.funded_amount, val.basket_amount);
				}
			});
		});
	}, 5000);
	
	function update_loan_table_item(id, name, use, status, sector, country, loan_amount, funded_amount, basket_amount){
		// Create a variable the stores the loan item from the DOM
		var loan_table_item = $('#loan-item-' + id);
		
		// Create a temporary variable to store the loan object
		var table_entry_temp = '';
		table_entry_temp += '<td>' + id + '</td>';
		table_entry_temp += '<td>' + name + '</td>';
		table_entry_temp += '<td>' + use + '</td>';
		table_entry_temp += '<td>' + capitalizeFirstLetter(status) + '</td>';
		table_entry_temp += '<td>' + sector + '</td>';
		table_entry_temp += '<td>' + country + '</td>';
		table_entry_temp += '<td>' + loan_amount + '</td>';
		table_entry_temp += '<td>' + funded_amount + '</td>';
		table_entry_temp += '<td>' + basket_amount + '</td>';
		
		// Replace the current table row item with the temporary variable
		loan_table_item.html(table_entry_temp);
	}
	
	
	function add_new_loan_table_item(id, name, use, status, sector, country, loan_amount, funded_amount, basket_amount){
		
		// Add it to loaded array
		LOADED_PROJECTS.push(id);
		
		// Create a temporary variable with table object details
		var table_entry_temp = '';
		table_entry_temp += '<tr id="loan-item-' + id + '">';
		table_entry_temp += '<td>' + id + '</td>';
		table_entry_temp += '<td>' + name + '</td>';
		table_entry_temp += '<td>' + use + '</td>';
		table_entry_temp += '<td>' + capitalizeFirstLetter(status) + '</td>';
		table_entry_temp += '<td>' + sector + '</td>';
		table_entry_temp += '<td>' + country + '</td>';
		table_entry_temp += '<td>' + loan_amount + '</td>';
		table_entry_temp += '<td>' + funded_amount + '</td>';
		table_entry_temp += '<td>' + basket_amount + '</td>';
		table_entry_temp += '</tr>';
		
		// Append the temporary variable to actual loans table
		$('#loans-table tbody').append(table_entry_temp);
	}
	
	function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
});